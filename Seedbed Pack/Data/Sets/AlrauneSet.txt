{
"vine1": {
"ID": "vine1",
"count": "1",
"group": "vine",
"icon": "vine_seedbed_preset",
"name": "Vine Set",
"script": "WHEN:turn
IF:dot,fire
dot,fire,1,1"
},
"vine2": {
"ID": "vine2",
"count": "2",
"group": "vine",
"icon": "vine_seedbed_preset",
"name": "Vine Set",
"script": "mulphyDMG,5
dodge,5
WHEN:turn
token_chance,10,stealth
IF:dot,fire
dot,fire,1,1"
},
"vine3": {
"ID": "vine3",
"count": "3",
"group": "vine",
"icon": "vine_seedbed_preset",
"name": "Vine Set",
"script": "mulphyDMG,10
dodge,5
WHEN:turn
token_chance,10,stealth
IF:dot,fire
dot,fire,2,1"
},
"vine4": {
"ID": "vine4",
"count": "4",
"group": "vine",
"icon": "vine_seedbed_preset",
"name": "Vine Set",
"script": "mulphyDMG,15
dodge,10
WHEN:turn
token_chance,20,stealth
IF:dot,fire
dot,fire,2,1"
},
"vine5": {
"ID": "vine5",
"count": "5",
"group": "vine",
"icon": "vine_seedbed_preset",
"name": "Vine Set",
"script": "mulphyDMG,20
dodge,10
WHEN:turn
token_chance,20,stealth
IF:dot,fire
dot,fire,2,2"
},
"vine6": {
"ID": "vine6",
"count": "6",
"group": "vine",
"icon": "vine_seedbed_preset",
"name": "Vine Set",
"script": "mulphyDMG,25
dodge,15
WHEN:turn
token_chance,30,stealth
IF:dot,fire
dot,fire,2,2"
}
}